# coding=utf-8
import pyautogui
from time import time, sleep
from sched import scheduler
import tempfile

# time.sleep(60.5)

x = 0
y = 0
pos_cursor = pyautogui.position()


class PyAutoGui:

    def get_cursor_position(self):
        # $print (pos_cursor)
        # x = pyautogui.position()
        # print(pyautogui.position())
        # print(pyautogui.position())
        self.x = str(pyautogui.position()[0])
        self.y = str(pyautogui.position()[1])
        self.result = self.x + "," + self.y
        # print(self.result)

        return self.result

    def timer_geral(self, interval, func):
        sch = scheduler(timefunc=time, delayfunc=sleep)

        vent = sch.enter(interval, 0, func, ())
        sch.run()
        # print("Aguardando...!")
        # print(self.get_cursor_position())
        return self.get_cursor_position()

    def event(self, x, y, interval, func, data):
        """
        :param x:largura
        :param y:altura
        :param interval:intervalo para execução
        :param func:nome da função a ser ezecutada
        :param func:data dados digitados no teclado

        :return:
        """
        sch = scheduler(timefunc=time, delayfunc=sleep)
        vent = sch.enter(interval, 0, func, (x, y, data,))
        sch.run()

    def click_contimaticPhoenix(self, x, y, data):
        """
        clica nas cordenadas fornecidas por x e y
        :param x: largura
        :param y: altura

        :return:
        """

        return pyautogui.click(x, y)

    def click_contimaticPhoenix_and_insert(self, x, y, data):
        """
        clica nas cordenadas fornecidas por x e y
        :param x: largura
        :param y: altura
        :return:
        """

        return pyautogui.click(x, y), pyautogui.typewrite(data)

# m =  PyAutoGui()

# m.timer_geral(8,m.get_cursor_position,)
# m.get_cursor_position()
#
##limpa tela
# m.event(2559, 1065, 1, m.click_contimaticPhoenix)
#
##chama contimatic
# m.click_contimaticPhoenix(471, 1071)
#
##clica no login e insere senha
# m.event(1328, 512, 6, m.click_contimaticPhoenix);pyautogui.typewrite("1712062889")
#
##clica no enter do botao login
# m.click_contimaticPhoenix(1378, 560)
#
#
#
##clica na bar superio
#
#
# m.event(592, 16, 8, m.click_contimaticPhoenix);pyautogui.hotkey('Ctrl','a');pyautogui.typewrite("31122018","0.05");pyautogui.hotkey('Alt','F4')
#
# m.event(1330, 346, 2, m.click_contimaticPhoenix)
#
# m.event(1278, 575, 2, m.click_contimaticPhoenix)
#
##clica na tab ativar empresa
##m.event(79, 52, 3, m.click_contimaticPhoenix);pyautogui.typewrite("31122018","tab","002")
#
##clica no campo competencia e altera
##m.event(79, 52, 3, m.click_contimaticPhoenix)
#
