# coding: utf-8
import os
from functools import partial
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.screenmanager import ScreenManager, Screen
from kivymd.textfields import MDTextField

from autogui import PyAutoGui
from kivy.properties import StringProperty, NumericProperty, ListProperty

Window.size = (615, 670)


class ScreenMain(ScreenManager):
    def screen_0(self):
        self.current = 'screen_0'

    def screen_1(self):
        self.current = 'screen_1'

    def screen_2(self):
        self.current = 'screen_2'

    def le_line(self, x, dir):
        """
        função para script.py
        :param x: name file
        :param y: name_data
        :return: le linha do arquivo e transforma em lista com split
        e retorna valor do segundo item como parametro
        """
        try:

            with open(dir + x, "r") as self.f:
                count = 0
                for line in self.f:
                    count += 1
                return count + 1
        except:
            return "1"

    def le_line_script(self, file, path):
        """

        :param file: nome do arquivo
        :param path: nome da pasta
        :return:
        """
        self.m = PyAutoGui()
        with open("data/" + file, "r") as self.f:
            for line in self.f:
                divide = line.split(",")
                print(divide[7])

                if divide[2] == "Click" and divide[5] == "":
                    self.m.event(int(divide[3]), int(divide[4]), float(divide[7]), self.m.click_contimaticPhoenix,
                                 data="null")
                elif divide[2] == "Click" and divide[5] != "":
                    self.m.event(int(divide[3]), int(divide[4]), float(divide[7]),
                                 self.m.click_contimaticPhoenix_and_insert, data=divide[5], )

    def grava_rotina(self, x):
        """

        :param x: string contendo valore das opçoes de cadastro de rotina
        :return:
        """
        self.divide = x.split(",")
        if self.check_file_exists("file", self.divide[0] + ".txt", "data/") == True:

            self.le_line(self.divide[0] + ".txt", "data/")
            self.create_file_line(x + "\n", self.divide[0], "data/")
            self.le_line(self.divide[0] + ".txt", "data/")
            print("arquivo existe")
            print(self.divide)

        elif self.check_file_exists("file", self.divide[0] + ".txt", "data/") == False:

            self.cria_arquivo_vazio(self.divide[0], "data/")
            self.create_file_line(x + "\n", self.divide[0], "data/")
            print("arquivo nao existe.....arquivo criado")
            print(self.divide)

    def cria_arquivo_vazio(self, file, dir):
        """
        :param x: nome do arquivo a ser crido
        :param dir: nome do diretorio
        :return:funcao cria arquivo vazio
        """
        with open(dir + file + ".txt", "w") as self.f:
            self.f.close()

    def mostra_posicao(self, time):
        time = int(time)
        self.m = PyAutoGui()
        return self.m.timer_geral(time, self.m.get_cursor_position)

    def create_file_line(self, x, y, dir):
        """
        :param x: valor da linha a ser adicionada
        :param y: nome do arquivo em que a linha será adicionada
        :return: adiciona linha no arquivo definido por parametro (y)
        :param dir: diretório do arquivo
        """
        y = dir + y + ".txt"
        x = str(x)
        try:
            self.arquivo_1 = open(y, 'r')
            self.conteudo = self.arquivo_1.readlines()
            self.conteudo.append(x)
            self.arquivo_1 = open(y, 'w')
            self.arquivo_1.writelines(self.conteudo)
        finally:
            self.arquivo_1.close()

    def check_file_exists(self, tip, name, path):
        """
        :param type: dir or file
        :param name: name file or = not_file
        :param path: path
        :return:
        """

        self.name = name
        self.path = path
        if os.path.isfile(self.path + self.name):
            print(self.path)
            return True
        else:
            return False


class Screen_0(Screen):
    pass


class Screen_1(Screen):
    pass


class Screen_2(Screen):
    pass


class Box(BoxLayout):
    pass


class TextUpeerMd(MDTextField):
    pass
    ## keyboard_suggestions = False
    #
    #def insert_text(self, substring, from_undo=False):
    #    s = substring.upper()
    #    self.multiline = False
    #    return super(TextUpeerMd, self).insert_text(s, from_undo=from_undo)



class MainApp(App):
    time = NumericProperty(3)
    pos = StringProperty("")
    rotina_0 = StringProperty("dddd")
    rotina_1 = StringProperty("")
    rotina_2 = StringProperty("")
    rotina_3 = StringProperty("")
    rotina_4 = StringProperty("")
    rotina_5 = StringProperty("")

    def build(self):
        self.root = ScreenMain()
        return self.root


MainApp().run()
